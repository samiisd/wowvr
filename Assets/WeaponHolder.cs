﻿using System;
using UnityEngine;


[RequireComponent(typeof(Animator))]
public class WeaponHolder : MonoBehaviour {
    public WeaponLogic weapon;

    private Transform weaponHandleTransform;

    private Transform handMain;
    private Transform handSecondary;
    
    private Animator _animator;

    void Start() {
        _animator = GetComponent<Animator>();
        
        // FIXME: should check main hand of the user (if VR)
        handMain = _animator.GetBoneTransform(HumanBodyBones.RightHand);
        handSecondary = _animator.GetBoneTransform(HumanBodyBones.LeftHand);


        weapon.transform.SetParent(handMain);
        // weaponTransform.localPosition = Vector3.zero;
        // weaponTransform.localRotation = Quaternion.identity;
        
        
        // Vector3 localHandlePosition = handMain.InverseTransformPoint(handleTransform.position);
        // Vector3 localHandleEulerAngles = handMain.InverseTransformDirection(handleTransform.eulerAngles);
        
        // weaponTransform.localPosition = localHandlePosition;
        // weaponTransform.localEulerAngles = localHandleEulerAngles;
        

    }

    private void Update() {
        Transform weaponTransform = weapon.transform;
        Transform handleTransform = weapon.handle;

        Vector3 dp = handMain.position - handleTransform.position;
        Quaternion dr = handMain.rotation * Quaternion.Inverse(handleTransform.rotation);
        
        weaponTransform.position += dp;
        weaponTransform.rotation = dr * weaponTransform.rotation;
    }
}
