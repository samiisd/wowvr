﻿using System;
using UnityEngine;

public class Movement : MonoBehaviour {
    public float speed = 1.0e-1f;
    
    private Animator _animator;
    private float _vHorizontal = 0.0f;
    private float _vForward = 0.0f;

    private void TriggerJump() {
        _animator.SetTrigger("jumped");
    }
    
    // Start is called before the first frame update
    void Start() {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            TriggerJump();
        }

        this._vHorizontal = Mathf.Lerp(this._vHorizontal, Input.GetAxis("Horizontal"), this.speed);
        this._vForward = Mathf.Lerp(this._vForward, Input.GetAxis("Vertical"), this.speed);

        _animator.SetFloat("v_horizontal", this._vHorizontal);
        _animator.SetFloat("v_forward", this._vForward);
    }
}
