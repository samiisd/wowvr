﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(OVRCameraRig))]
public class IKTracking : MonoBehaviour {
    public OVRCameraRig rigCamera;
    
    private Animator _animator;
    private bool _isMouseController;

    // Start is called before the first frame update
    void Start() {
        _animator = GetComponent<Animator>();
        _isMouseController = !OVRInput.IsControllerConnected(OVRInput.Controller.RTouch);
    }

    private Vector3 GetMouseTargetPosition(Camera cam) {
        float width = cam.scaledPixelWidth;
        float height = cam.scaledPixelHeight;
        
        Vector3 headPosition = _animator.GetBoneTransform(HumanBodyBones.Head).position;
        
        float x = 2f * Mathf.Clamp(Input.mousePosition.x, 0.0f, width) / width - 1f;
        float y = 2f * Mathf.Clamp(Input.mousePosition.y, 0.0f, height) / height - 1f;
            
        Vector3 mousePosition =
            new Vector3(
                1.5f*x,
                1.5f*y,
                1.0f
        ) + Vector3.up * headPosition.y;
        
        return transform.TransformPoint(mousePosition);
    }

    
    private void HandleIKMouse() {
        if (Camera.main is null) {
            return;
        }

        Vector3 target = GetMouseTargetPosition(Camera.main);
        
        if (Input.GetButton("Fire1")) {
            _animator.SetIKPosition(AvatarIKGoal.LeftHand, target);
        }
        if (Input.GetButton("Fire2")) {
            _animator.SetIKPosition(AvatarIKGoal.RightHand, target);
        }
        if (Input.GetButton("Fire3")) {
            _animator.SetLookAtWeight(1.0f);
            _animator.SetLookAtPosition(target);
        } else {
            _animator.SetLookAtWeight(0f);
        }
    }

    private void HandleIKVr() {
        _animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f); 
        _animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f); 
        if (OVRInput.GetControllerPositionValid(OVRInput.Controller.RTouch)) {
            Vector3 position = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
            _animator.SetIKPosition(AvatarIKGoal.RightHand, position);
        }
        
        if (OVRInput.GetControllerPositionValid(OVRInput.Controller.LTouch)) {
            Vector3 position = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
            _animator.SetIKPosition(AvatarIKGoal.LeftHand, position);
        }
        
        if (OVRInput.GetControllerOrientationValid(OVRInput.Controller.LTouch)) {
            Quaternion rotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);
            _animator.SetIKRotation(AvatarIKGoal.LeftHand, rotation);
        }

        if (OVRInput.GetControllerOrientationValid(OVRInput.Controller.RTouch)) {
            Quaternion rotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);
            _animator.SetIKRotation(AvatarIKGoal.RightHand, rotation);
        }
        
        _animator.SetLookAtWeight(1.0f);
        _animator.SetLookAtPosition(
            _animator.GetBoneTransform(HumanBodyBones.Head).position + rigCamera.transform.forward
        );
    }
    private void OnAnimatorIK(int layerIndex) {
        _animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f); 
        _animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f); 
        
        if (_isMouseController) {
            HandleIKMouse();
        } else {
            HandleIKVr();
        }
    }
}
